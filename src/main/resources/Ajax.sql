-- DROP DATABASE Ajax;
CREATE DATABASE IF NOT EXISTS Ajax
USE Ajax;

CREATE TABLE IF NOT EXISTS `room` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
   PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = ascii;

LOCK TABLES `room` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
INSERT INTO `room` VALUES (1,'Director');
INSERT INTO `room` VALUES (2,'Accountant');
INSERT INTO `room` VALUES (3,'Security Director');
/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;

CREATE TABLE IF NOT EXISTS `zone` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `access_level` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

LOCK TABLES `zone` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
INSERT INTO `zone` VALUES (1,'Office');
INSERT INTO `zone` VALUES (2,'Security');
INSERT INTO `zone` VALUES (3,'Warehouse');
/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;

CREATE TABLE IF NOT EXISTS `sensor` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `zone_id` INT NOT NULL,
  `type` VARCHAR(45) NULL,
  `quantity` INT NULL,
  PRIMARY KEY (`id`, `zone_id`),
  INDEX `fk_sensor_zone1_idx` (`zone_id` ASC),
  CONSTRAINT `fk_sensor_zone1`
    FOREIGN KEY (`zone_id`)
    REFERENCES `zone` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

LOCK TABLES `sensor` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
INSERT INTO `sensor` VALUES (1,1,'Smoke detector',6);
INSERT INTO `sensor` VALUES (2,2,'Gas detector',6);
INSERT INTO `sensor` VALUES (3,3,'Move detector',5);
/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;


CREATE TABLE IF NOT EXISTS `employee` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `employee_info_id` INT NULL DEFAULT NULL,
  `first_name` VARCHAR(50) NULL,
  `second_name` VARCHAR(50) NULL,
  `job_status` VARCHAR(50) NULL,
  `employee_id` INT NOT NULL,
  `employee_employee_info_id` INT NOT NULL,
  PRIMARY KEY (`id`, `employee_info_id`),
  INDEX `fk_employee_employee1_idx` (`employee_id` ASC, `employee_employee_info_id` ASC),
  CONSTRAINT `fk_employee_employee1`
    FOREIGN KEY (`employee_id` , `employee_employee_info_id`)
    REFERENCES `employee` (`id` , `employee_info_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
INSERT INTO `employee` VALUES (1,'John','Prestige','Director',1,1);
INSERT INTO `employee` VALUES (2,'Kevin', 'Bricks','Accountant',2,2);
INSERT INTO `employee` VALUES (3,'Richard','Parkins','Security Director',3,3);
/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;
