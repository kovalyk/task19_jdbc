package com.epam.service;

import com.epam.DAO.implementation.RoomDaoImpl;
import com.epam.model.entity.Room;

import java.sql.SQLException;
import java.util.List;

public class RoomService {

    public List<Room> findAll() throws SQLException {
        return new RoomDaoImpl().findAll();
    }

    public Room findById(String id) throws SQLException {
        return new RoomDaoImpl().findById(id);
    }

    public int create(Room entity) throws SQLException {
        return new RoomDaoImpl().create(entity);
    }

    public int update(Room entity) throws SQLException {
        return new RoomDaoImpl().update(entity);
    }

    public int delete(String id) throws SQLException {
        return new RoomDaoImpl().delete(id);
    }

//    public int deleteWithMoveOfEmployees(String idDeleted, String idMoveTo) throws SQLException {
//        int deletedAmount = 0;
//        Connection connection = ConnectionManager.getConnection();
//        try {
//            connection.setAutoCommit(false);
//            if (new DepartmentDaoImpl().findById(idMoveTo) == null)
//                throw new SQLException();
//
//            List<EmployeeEntity> employees = new EmployeeDaoImpl().findByDeptNo(idDeleted);
//            for (EmployeeEntity entity : employees) {
//                entity.setDeptNo(idMoveTo);
//                new EmployeeDaoImpl().update(entity);
//            }
//            deletedAmount = new DepartmentDaoImpl().delete(idDeleted);
//            connection.commit();
//        } catch (SQLException e) {
//            if (connection != null) {
//                System.err.print("Transaction is being rolled back");
//                connection.rollback();
//            }
//        } finally {
//            connection.setAutoCommit(true);
//        }
//        return deletedAmount;
//    }
}
