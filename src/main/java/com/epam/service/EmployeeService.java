package com.epam.service;

import com.epam.DAO.implementation.EmployeeDaoImpl;
import com.epam.model.entity.Employee;

import java.sql.SQLException;
import java.util.List;

public class EmployeeService {
    public List<Employee> findAll() throws SQLException {
        return new EmployeeDaoImpl().findAll();
    }

    public Employee findById(Integer id) throws SQLException {
        return new EmployeeDaoImpl().findById(id);
    }

    public int create(Employee entity) throws SQLException {
        return new EmployeeDaoImpl().create(entity);
    }

    public int update(Employee entity) throws SQLException {
        return new EmployeeDaoImpl().update(entity);
    }

    public int delete(Integer id) throws SQLException {
        return new EmployeeDaoImpl().delete(id);
    }

    public List<Employee> findByName(String name) throws SQLException {
        return new EmployeeDaoImpl().findByName(name);
    }
}
