package com.epam.service;

import com.epam.DAO.implementation.ZoneDaoImpl;
import com.epam.model.entity.Zone;

import java.sql.SQLException;
import java.util.List;

public class ZoneService {
    public List<Zone> findAll() throws SQLException {
        return new ZoneDaoImpl().findAll();
    }

   public Zone findById(Integer id) throws SQLException{
       return new ZoneDaoImpl().findById(id);
    }

    public int create(Zone entity) throws SQLException{
        return new ZoneDaoImpl().create(entity);
    }

    public int update(Zone entity) throws SQLException{
        return new ZoneDaoImpl().update(entity);
    }

    public int delete(Integer id) throws SQLException{
        return new ZoneDaoImpl().delete(id);
    }
}
