package com.epam.service;

import com.epam.DAO.implementation.SensorDaoImpl;
import com.epam.model.entity.Sensor;

import java.sql.SQLException;
import java.util.List;

public class SensorService {
    public List<Sensor> findAll() throws SQLException {
        return new SensorDaoImpl().findAll();
    }

    public int create(Sensor entity) throws SQLException{
        return new SensorDaoImpl().create(entity);
    }

    public int update(Sensor entity) throws SQLException{
        return new SensorDaoImpl().update(entity);
    }

    public int delete(Integer id) throws SQLException{
        return new SensorDaoImpl().delete(id);
    }
}
