package com.epam.DAO;

import com.epam.model.entity.Zone;

public interface ZoneDAO extends GeneralDAO<Zone, Integer> {
}
