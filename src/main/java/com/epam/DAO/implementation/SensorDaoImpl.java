package com.epam.DAO.implementation;

import com.epam.DAO.SensorDAO;
import com.epam.model.entity.Sensor;
import com.epam.persistent.ConnectionManager;
import com.epam.transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SensorDaoImpl implements SensorDAO {
    private static final String FIND_ALL = "SELECT * FROM sensor";
    private static final String DELETE = "DELETE FROM sensor WHERE id=? AND project_no=?";
    private static final String CREATE = "INSERT INTO sensor (id, type) VALUES (?, ?)";
    private static final String UPDATE = "UPDATE sensor SET type=? WHERE id=?";

    @Override
    public List<Sensor> findAll() throws SQLException {
        List<Sensor> works = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    works.add((Sensor) new Transformer(Sensor.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return works;
    }

    @Override
    public Sensor findById(Integer id) throws SQLException {
        return null;
    }

    @Override
    public int create(Sensor entity) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(CREATE)) {
            ps.setInt(1, entity.getSensorId());
            ps.setString(2, entity.getSensorType());
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(Sensor entity) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(UPDATE)) {
            ps.setString(1, entity.getSensorType());
            ps.setInt(2, entity.getSensorId());
            return ps.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(DELETE)) {
            ps.setInt(1, id);
            return ps.executeUpdate();
        }
    }
}
