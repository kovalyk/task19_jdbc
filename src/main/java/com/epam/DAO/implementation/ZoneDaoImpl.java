package com.epam.DAO.implementation;

import com.epam.DAO.ZoneDAO;
import com.epam.model.entity.Zone;
import com.epam.persistent.ConnectionManager;
import com.epam.transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ZoneDaoImpl implements ZoneDAO {
    private static final String FIND_ALL = "SELECT * FROM zone";
    private static final String DELETE = "DELETE FROM zone WHERE id=?";
    private static final String CREATE = "INSERT INTO zone (id, name) VALUES (?, ?)";
    private static final String UPDATE = "UPDATE zone SET name=? WHERE id=?";
    private static final String FIND_BY_ID = "SELECT * FROM zone WHERE id=?";

    @Override
    public List<Zone> findAll() throws SQLException {
        List<Zone> projects = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    projects.add((Zone) new Transformer(Zone.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return projects;
    }

    @Override
    public Zone findById(Integer id) throws SQLException {
        Zone entity = null;
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)) {
            ps.setInt(1, id);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    entity = (Zone) new Transformer(Zone.class).fromResultSetToEntity(resultSet);
                    break;
                }
            }
        }
        return entity;
    }

    @Override
    public int create(Zone entity) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(CREATE)) {
            ps.setInt(1, entity.getZoneId());
            ps.setString(2, entity.getZoneName());
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(Zone entity) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(UPDATE)) {
            ps.setString(1, entity.getZoneName());
            ps.setInt(2, entity.getZoneId());
            return ps.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(DELETE)) {
            ps.setInt(1, id);
            return ps.executeUpdate();
        }
    }
}

