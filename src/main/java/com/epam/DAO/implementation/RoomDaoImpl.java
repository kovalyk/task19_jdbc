package com.epam.DAO.implementation;

import com.epam.DAO.RoomDAO;
import com.epam.model.entity.Room;
import com.epam.persistent.ConnectionManager;
import com.epam.transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class RoomDaoImpl implements RoomDAO {
    private static final String FIND_ALL = "SELECT * FROM room";
    private static final String DELETE = "DELETE FROM room WHERE id=?";
    private static final String CREATE = "INSERT INTO room (id, name) VALUES (?, ?)";
    private static final String UPDATE = "UPDATE room SET name=? WHERE id=?";
    private static final String FIND_BY_ID = "SELECT * FROM room WHERE id=?";


    @Override
    public List<Room> findAll() throws SQLException {
        List<Room> departments = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    departments.add((Room) new Transformer(Room.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return departments;
    }

    @Override
    public Room findById(String id) throws SQLException {
        Room entity = null;
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)) {
            ps.setString(1, id);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    entity = (Room) new Transformer(Room.class).fromResultSetToEntity(resultSet);
                    break;
                }
            }
        }
        return entity;
    }

    @Override
    public int create(Room entity) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(CREATE)) {
            ps.setInt(1, entity.getRoomId());
            ps.setString(2, entity.getRoomName());
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(Room entity) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(UPDATE)) {
            ps.setInt(1, entity.getRoomId());
            ps.setString(2, entity.getRoomName());
            return ps.executeUpdate();
        }
    }

    @Override
    public int delete(String id) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(DELETE)) {
            ps.setString(1, id);
            return ps.executeUpdate();
        }
    }
}
