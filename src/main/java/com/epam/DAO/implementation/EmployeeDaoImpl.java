package com.epam.DAO.implementation;

import com.epam.DAO.EmployeeDAO;
import com.epam.model.entity.Employee;
import com.epam.persistent.ConnectionManager;
import com.epam.transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EmployeeDaoImpl implements EmployeeDAO {
    private static final String FIND_ALL = "SELECT * FROM employee";
    private static final String DELETE = "DELETE FROM employee WHERE emp_no=?";
    private static final String CREATE = "INSERT INTO employee (id, first_name, second_name, job_status) VALUES (?, ?, ?, ?)";
    private static final String UPDATE = "UPDATE employee SET first_name=?, second_name=?, job_status=? WHERE id=?";
    private static final String FIND_BY_ID = "SELECT * FROM employee WHERE id=?";
    private static final String FIND_BY_FIRST_NAME = "SELECT * FROM employee WHERE first_name=?";

    @Override
    public List<Employee> findAll() throws SQLException {
        List<Employee> employees = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    employees.add((Employee) new Transformer(Employee.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return employees;
    }

    @Override
    public Employee findById(Integer id) throws SQLException {
        Employee entity=null;
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)) {
            ps.setInt(1,id);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    entity=(Employee)new Transformer(Employee.class).fromResultSetToEntity(resultSet);
                    break;
                }
            }
        }
        return entity;
    }

    @Override
    public int create(Employee entity) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(CREATE)) {
            ps.setInt(1,entity.getEmpId());
            ps.setString(2,entity.getFirstName());
            ps.setString(3,entity.getSecondName());
            ps.setString(4,entity.getJobStatus());
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(Employee entity) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(UPDATE)) {
            ps.setInt(4,entity.getEmpId());
            ps.setString(1,entity.getFirstName());
            ps.setString(2,entity.getSecondName());
            ps.setString(3,entity.getJobStatus());
            return ps.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(DELETE)) {
            ps.setInt(1,id);
            return ps.executeUpdate();
        }
    }

    @Override
    public List<Employee> findByName(String name) throws SQLException {
        List<Employee> employees = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_FIRST_NAME)) {
            ps.setString(1,name);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    employees.add((Employee) new Transformer(Employee.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return employees;
    }
}
