package com.epam.DAO;

import com.epam.model.entity.Employee;

import java.sql.SQLException;
import java.util.List;

public interface EmployeeDAO extends GeneralDAO<Employee, Integer> {
    List<Employee> findByName(String firstName) throws SQLException;
}
