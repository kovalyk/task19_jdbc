package com.epam.DAO;

import com.epam.model.entity.Sensor;

public interface SensorDAO extends GeneralDAO<Sensor, Integer> {
}
