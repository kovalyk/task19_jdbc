package view;

import com.epam.model.entity.Employee;
import com.epam.model.entity.Room;
import com.epam.model.entity.Sensor;
import com.epam.model.entity.Zone;
import com.epam.service.EmployeeService;
import com.epam.service.RoomService;
import com.epam.service.SensorService;
import com.epam.service.ZoneService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;


public class MyView {
    private static Logger log = LogManager.getLogger(MyView.class);
    private static Scanner input = new Scanner(System.in);
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;

    public MyView() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();
        menu.put("A", "   A - Select all table");
        menu.put("B", "   B - Select structure of DB");

        menu.put("1", "   1 - Table: Room");
        menu.put("11", "  11 - Create for Room");
        menu.put("12", "  12 - Update Room");
        menu.put("13", "  13 - Delete from Room");
        menu.put("14", "  14 - Select Room");
        menu.put("15", "  15 - Find Room by ID");

        menu.put("2", "   2 - Table: Employee");
        menu.put("21", "  21 - Create for Employee");
        menu.put("22", "  22 - Update Employee");
        menu.put("23", "  23 - Delete from Employee");
        menu.put("24", "  24 - Select Employee");
        menu.put("25", "  25 - Find Employee by ID");
        menu.put("26", "  26 - Find Employee by Name");

        menu.put("3", "   3 - Table: Zone");
        menu.put("31", "  31 - Create for Zone");
        menu.put("32", "  32 - Update Zone");
        menu.put("33", "  33 - Delete from Zone");
        menu.put("34", "  34 - Select Zone");
        menu.put("35", "  35 - Find Project by ID");

        menu.put("4", "   4 - Table: Sensor");
        menu.put("41", "  41 - Create for Sensor");
        menu.put("42", "  42 - Update Sensor");
        menu.put("43", "  43 - Delete from Sensor");

        menu.put("Q", "   Q - exit");

        methodsMenu.put("A", this::selectAllTable);

        methodsMenu.put("11", this::createForRoom);
        methodsMenu.put("12", this::updateRoom);
        methodsMenu.put("13", this::deleteFromRoom);
        methodsMenu.put("14", this::selectRoom);
        methodsMenu.put("15", this::findRoomByID);

        methodsMenu.put("21", this::createForEmployee);
        methodsMenu.put("22", this::updateEmployee);
        methodsMenu.put("23", this::deleteFromEmployee);
        methodsMenu.put("24", this::selectEmployee);
        methodsMenu.put("25", this::findEmployeeByID);
        methodsMenu.put("26", this::findEmployeeByName);

        methodsMenu.put("31", this::createForZone);
        methodsMenu.put("32", this::updateZone);
        methodsMenu.put("33", this::deleteFromZone);
        methodsMenu.put("34", this::selectZone);
        methodsMenu.put("35", this::findZoneByID);

        methodsMenu.put("41", this::createForSensor);
        methodsMenu.put("42", this::updateSensor);
        methodsMenu.put("43", this::deleteFromSensor);
    }

    private void selectAllTable() throws SQLException {
        selectRoom();
        selectEmployee();
        selectZone();
        selectSensor();
    }

    private void deleteFromRoom() throws SQLException {
        log.info("Input ID for Room: ");
        String id = input.nextLine();
        RoomService roomService = new RoomService();
        int count = roomService.delete(id);
        log.warn("There are deleted %d rows\n", count);
    }

    private void createForRoom() throws SQLException {
        log.info("Input ID for Room: ");
        Integer id = input.nextInt();
        log.info("Input dept_name for Room: ");
        String roomName = input.nextLine();
        Room entity = new Room(id, roomName);

        RoomService roomService = new RoomService();
        int count = roomService.create(entity);
        log.info("There are created %d rows\n", count);
    }

    private void updateRoom() throws SQLException {
        log.info("Input ID(dept_no) for Room: ");
        Integer id = input.nextInt();
        log.info("Input name for Room: ");
        String roomName = input.next();
        Room entity = new Room(id, roomName);

        RoomService roomService = new RoomService();
        int count = roomService.update(entity);
        log.warn("There are updated %d rows\n", count);
    }

    private void selectRoom() throws SQLException {
        log.info("\nTable: Department");
        RoomService roomService = new RoomService();
        List<Room> rooms = roomService.findAll();
        for (Room entity : rooms) {
            log.warn(entity);
        }
    }

    private void findRoomByID() throws SQLException {
        log.info("Input ID(dept_no) for Room: ");
        String id = input.nextLine();
        RoomService roomService = new RoomService();
        Room entity = roomService.findById(id);
        log.warn(entity);
    }

    //------------------------------------------------------------------------

    private void deleteFromEmployee() throws SQLException {
        log.info("Input ID(dept_no) for Employee: ");
        Integer id = input.nextInt();
        input.nextLine();
        EmployeeService employeeService = new EmployeeService();
        int count = employeeService.delete(id);
        log.warn("There are deleted %d rows\n", count);
    }

    private void createForEmployee() throws SQLException {
        log.info("Input ID(epm_no) for Employee: ");
        Integer id = input.nextInt();
        input.nextLine();
        log.info("Input first name for Employee: ");
        String firstName = input.nextLine();
        log.info("Input second name for Employee: ");
        String SecondName = input.nextLine();
        log.info("Input job status for Employee: ");
        String jobStatus = input.nextLine();
        Employee entity = new Employee(id, firstName, SecondName, jobStatus);
        EmployeeService employeeService = new EmployeeService();

        int count = employeeService.create(entity);
        log.warn("There are created %d rows\n", count);
    }

    private void updateEmployee() throws SQLException {
        log.info("Input ID for Employee: ");
        Integer id = input.nextInt();
        input.nextLine();
        log.info("Input first name for Employee: ");
        String firstName = input.nextLine();
        log.info("Input second name for Employee: ");
        String SecondName = input.nextLine();
        log.info("Input job status for Employee: ");
        String jobStatus = input.nextLine();
        Employee entity = new Employee(id, firstName, SecondName, jobStatus);
        EmployeeService employeeService = new EmployeeService();

        int count = employeeService.update(entity);
        log.warn("There are updated %d rows\n", count);
    }

    private void selectEmployee() throws SQLException {
        log.info("\nTable: Employee");
        EmployeeService employeeService = new EmployeeService();
        List<Employee> employees = employeeService.findAll();
        for (Employee entity : employees) {
            log.warn(entity);
        }
    }

    private void findEmployeeByID() throws SQLException {
        log.info("Input ID for Employee: ");
        Integer id = input.nextInt();
        input.nextLine();
        EmployeeService employeeService = new EmployeeService();
        Employee entity = employeeService.findById(id);
        log.warn(entity);
    }

    private void findEmployeeByName() throws SQLException {
        log.info("Input First Name for Employee: ");
        String firstName = input.nextLine();
        EmployeeService employeeService = new EmployeeService();
        List<Employee> employees = employeeService.findByName(firstName);
        for (Employee entity : employees) {
            log.warn(entity);
        }
    }

    //------------------------------------------------------------------------

    private void updateZone() throws SQLException {
        log.info("Input ID for Zone: ");
        Integer id = input.nextInt();
        log.info("Input project_name for Zone: ");
        String zoneName = input.nextLine();
        input.nextLine();
        Zone entity = new Zone(id, zoneName);

        ZoneService projectService = new ZoneService();
        int count = projectService.update(entity);
        log.warn("There are updated %d rows\n", count);
    }

    private void deleteFromZone() throws SQLException {
        log.info("Input ID for Zone: ");
        Integer id = input.nextInt();
        ZoneService zoneService = new ZoneService();
        int count = zoneService.delete(id);
        log.warn("There are deleted %d rows\n", count);
    }

    private void createForZone() throws SQLException {
        log.info("Input ID for Zone: ");
        Integer id = input.nextInt();
        log.info("Input type for Zone: ");
        String zoneName = input.nextLine();
        input.nextLine();
        Zone entity = new Zone(id, zoneName);

        ZoneService zoneService = new ZoneService();
        int count = zoneService.create(entity);
        log.warn("There are created %d rows\n", count);
    }

    private void selectZone() throws SQLException {
        log.info("\nTable: Zone");
        ZoneService projectService = new ZoneService();
        List<Zone> projects = projectService.findAll();
        for (Zone entity : projects) {
            System.out.println(entity);
        }
    }

    private void findZoneByID() throws SQLException {
        log.info("Input ID for Zone: ");
        Integer id = input.nextInt();
        ZoneService zoneService = new ZoneService();
        Zone entity = zoneService.findById(id);
        log.warn(entity);
    }

    //------------------------------------------------------------------------

    private void deleteFromSensor() throws SQLException {
        log.info("Input ID for Sensor: ");
        Integer id = input.nextInt();
        SensorService sensorService = new SensorService();
        int count = sensorService.delete(id);
        log.warn("There are deleted %d rows\n", count);
    }

    private void createForSensor() throws SQLException {

        log.info("Input ID for Sensor: ");
        Integer id = input.nextInt();
        log.info("Input type for Sensor: ");
        String type = input.nextLine();
        Sensor entity = new Sensor(id, type);

        SensorService sensorService = new SensorService();
        int count = sensorService.create(entity);
        log.warn("There are created %d rows\n", count);
    }

    private void updateSensor() throws SQLException {
        log.info("Input ID for Sensor: ");
        Integer id = input.nextInt();
        log.info("Input type for Sensor: ");
        String type = input.next();
        Sensor entity = new Sensor(id, type);

        SensorService sensorService = new SensorService();
        int count = sensorService.update(entity);
        log.warn("There are updated %d rows\n", count);
    }

    private void selectSensor() throws SQLException {
        log.info("\nTable: Sensor");
        SensorService sensorService = new SensorService();
        List<Sensor> sensors = sensorService.findAll();
        for (Sensor entity : sensors) {
            log.warn(entity);
        }
    }
    //-------------------------------------------------------------------------

    private void outputMenu() {
        log.info("\nMENU:");
        for (String key : menu.keySet())
            if (key.length() == 1) System.out.println(menu.get(key));
    }

    private void outputSubMenu(String fig) {

        log.info("\nSubMENU:");
        for (String key : menu.keySet())
            if (key.length() != 1 && key.substring(0, 1).equals(fig)) System.out.println(menu.get(key));
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            log.info("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();

            if (keyMenu.matches("^\\d")) {
                outputSubMenu(keyMenu);
                log.info("Please, select menu point.");
                keyMenu = input.nextLine().toUpperCase();
            }

            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
